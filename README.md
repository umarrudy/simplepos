
# Simple Point of Sale (POS) & Inventory System with Laravel

## Getting Started

### Installation

- `git clone https://gitlab.com/umarrudy/simplepos.git`
- `cd simplepos/`
- `composer install`
- `cp .env.example .env`
- Update `.env` and set database credentials
- `php artisan key:generate`
- `php artisan migrate`
- `php artisan db:seed`
- `php artisan passport:install`
- `npm install`
- `npm run dev`
- `php artisan serve`

### Screenshot

![login](/public/screenshot/login.png "Login Screen")
![dash](/public/screenshot/dashboard.png "Dashboard")
![stat](/public/screenshot/stats.png "Company Stats")
![add-new-sn](/public/screenshot/add-new-sn.png "Add New Serial Number")
![sn-list](/public/screenshot/sn-list.png "Serial Number List")
![category-list](/public/screenshot/category-list.png "List of Category")
![customer-list](/public/screenshot/customer-list.png "List of Customers")
![dash-filter](/public/screenshot/dash-filter.png "Filter Dashboard by Date")
![items-list](/public/screenshot/items-list.png "List of Items")
![make-invoice](/public/screenshot/make-invoice.png "Make New Invoice")
![make-user](/public/screenshot/make-user.png "Make New User")
![users-list](/public/screenshot/users-list.png "List of Users")

